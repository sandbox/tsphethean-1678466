<?php

/**
 * @file
 * Provides callbacks from queue_manager menu items.
 */

/**
 * View all the items in a queue.
 */
function queue_manager_view_queue($queue_name) {
  $query = db_select('queue', 'q')
    ->fields('q', array('item_id', 'expire', 'created'))
    ->condition('q.name', $queue_name)
    ->extend('PagerDefault')
    ->limit(25)
    ->execute();

  foreach ($query as $record) {
    $result[] = $record;
  }

  $header = array(
    t('Item ID'),
    t('Expires'),
    t('Created'),
    array('data' => t('Operations'), 'colspan' => '3'),
  );

  $rows = array();
  foreach ($result as $item) {
    $row = array();
    $row[] = $item->item_id;
    $row[] = ($item->expire ? date(DATE_RSS, $item->expire) : $item->expire);
    $row[] = date(DATE_RSS, $item->created);
    $row[] = l(t('View'), "admin/config/system/queue_manager/$queue_name/view/$item->item_id");
    $row[] = l(t('Release'), "admin/config/system/queue_manager/$queue_name/release/$item->item_id");
    $row[] = l(t('Delete'), "admin/config/system/queue_manager/$queue_name/delete/$item->item_id");
    $rows[] = array('data' => $row);
  }

  $content = theme('table', array('header' => $header, 'rows' => $rows));
  $content .= theme('pager');

  return $content;
}


/**
 * Display the details of an individual queue item.
 */
function queue_manager_view_queue_item($queue_item) {

  $rows[] = array(
    'data' => array(
      'header' => t('Item ID'),
      'data' => $queue_item->item_id,
    ),
  );
  $rows[] = array(
    'data' => array(
      'header' => t('Queue name'),
      'data' => $queue_item->name,
    ),
  );
  $rows[] = array(
    'data' => array(
      'header' => t('Expire'),
      'data' => ($queue_item->expire ? date(DATE_RSS, $queue_item->expire) : $queue_item->expire),
    ),
  );
  $rows[] = array(
    'data' => array(
      'header' => t('Created'),
      'data' => date(DATE_RSS, $queue_item->created),
    ),
  );
  $rows[] = array(
    'data' => array(
      'header' => array('data' => t('Data'), 'style' => 'vertical-align:top'),
      'data' => '<pre>' . print_r(unserialize($queue_item->data), TRUE) . '</pre>',
      // @TODO - should probably do something nicer than print_r here...
    ),
  );

  return theme('table', array('rows' => $rows));
}

/**
 * Releases an item from its queue by setting expiry to zero).
 *
 * @param object $queue_item
 *   The queue item to release.
 *
 * @see queue_manager_release_item_submit()
 */
function queue_manager_release_item_form($form, $form_state, $queue_item) {
  return confirm_form(
    array(
      'queue_item' => array(
        '#type' => 'value',
        '#value' => $queue_item,
      ),
    ),
    t('Are you sure you want to release queue item %queue_item?', array('%queue_item' => $queue_item->item_id)),
    "admin/config/system/queue_manager/$queue_item->name/view",
    t('This action cannot be undone and will force the release of the item even if it is currently being processed.'),
    t('Release item'),
    t('Cancel')
  );
}

/**
 * Form submission handler for queue_manager_release_item_form().
 *
 * Removes all items from a feed and redirects to the overview page.
 *
 * @see queue_manager_release_item()
 */
function queue_manager_release_item_form_submit($form, &$form_state) {
  module_load_include('inc', 'queue_manager', 'queue_manager.api');
  $queue_item = $form_state['values']['queue_item'];
  queue_manager_release_item($queue_item->item_id);
  $form_state['redirect'] = "admin/config/system/queue_manager/$queue_item->name/view";
}


/**
 * Releases an item from its queue by setting expiry to zero).
 *
 * @param object $queue_item
 *   The queue item to release.
 *
 * @see queue_manager_delete_item_submit()
 */
function queue_manager_delete_item_form($form, $form_state, $queue_item) {
  return confirm_form(
    array(
      'queue_item' => array(
        '#type' => 'value',
        '#value' => $queue_item,
      ),
    ),
    t('Are you sure you want to delete queue item %queue_item?', array('%queue_item' => $queue_item->item_id)),
    "admin/config/system/queue_manager/$queue_item->name/view",
    t('This action cannot be undone and will force the deletion of the item even if it is currently being processed.'),
    t('Delete item'),
    t('Cancel')
  );
}

/**
 * Form submission handler for queue_manager_delete_item_form().
 *
 * Removes all items from a feed and redirects to the overview page.
 *
 * @see queue_manager_delete_item()
 */
function queue_manager_delete_item_form_submit($form, &$form_state) {
  module_load_include('inc', 'queue_manager', 'queue_manager.api');
  $queue_item = $form_state['values']['queue_item'];
  queue_manager_delete_item($queue_item->item_id);
  $form_state['redirect'] = "admin/config/system/queue_manager/$queue_item->name/view";
}
