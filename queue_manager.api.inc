<?php

/**
 * @file
 * API for interacting with Drupal Queue entries safely.
 */

/**
 * Select distinct list of queue names from the queue table.
 *
 * @return array
 *   An array of queues
 */
function queue_manager_get_queues() {
  // Select distinct list of queue names from the queue table.
  $query = db_select('queue', 'q')
  ->fields('q', array('name'))
  ->distinct()
  ->execute();

  foreach ($query as $record) {
    $result[] = $record->name;
  }

  return $result;
}

/**
 * Release the specified item from its queue - set expire to zero.
 *
 * @param int $item_id
 *   The item_id value of the queue item to be released.
 */
function queue_manager_release_item($item_id) {
  // @todo: try... catch...
  drupal_set_message("Released queue item " . $item_id);

  db_update('queue')
    ->condition('item_id', $item_id)
    ->fields(array('expire' => 0))
    ->execute();
}


/**
 * Delete the specified item from its queue by setting expire value to zero.
 *
 * @param int $item_id
 *   The item_id value of the queue item to be released.
 */
function queue_manager_delete_item($item_id) {
  // @todo: try... catch...
  drupal_set_message("Deleted queue item " . $item_id);

  db_delete('queue')
    ->condition('item_id', $item_id)
    ->execute();
}
