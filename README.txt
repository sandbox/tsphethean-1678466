Queue Manager
=============
The Queue Manager module provides an administrative interface to inspect and
manage Drupal Database Queues.


Why Queue Manager
=================
Sometimes code has bugs. If this code is processing your queues then items can
become stuck and remain unprocessed. The Queue Manager module allows direct
interaction with queue items for inspection and overriding queue processing.

Queue Manager is intended as a debugging/analysis tool, and provides access to
manage Drupal Database queues without having to write the SQL statements
yourself every time.

Queue Manager should be used with caution, as it gives you the ability to delete
queue items which may be being processed. As such, the "admin queue_manager"
permission created by the module should only be granted to roles with advanced
site privileges.


Queue Manager functionality
=======================
Queue Manager allows you to:
* View all Database Queues in your Drupal database
* View the details of individual queue items in a queue
* Force the releasing of individual queue items in a queue
* Force the deletion of individual queue item in a queue

Queue Manager does not provide any functionality for any queue backends other
than Drupal Database Queues. For Queue backends such as ActiveMQ or Beanstalkd
this functionality is provided by the queueing software itself.

Using Queue Manager
===================
Queue manager can be accessed at
http://<yoursite>/admin/config/system/queue_manager


Alternative modules
===================

Queue UI - http://drupal.org/project/queue_ui

Queue UI uses queues which have been identified via hook_queue_info() and are
processed by Drupal Cron. It allows queues to be processed and deleted, but
doesn't allow the forced deletion or releasing of individual items outside of
Queue API.
